Visualization of European FIRs (those starting w/ 'E', 'L' or 'UK', 'UG', 'UD', 'BK', 'GC') and FABs (Functional Air Blocks).

Click on FIR/FAB to zoom in/out.

Geometries from the [Network Manager](https://www.eurocontrol.int/network-manager) and available in [this repo](https://github.com/espinielli/firs).

Built with [blockbuilder.org](http://blockbuilder.org)

forked from <a href='http://bl.ocks.org/espinielli/'>espinielli</a>'s block: <a href='http://bl.ocks.org/espinielli/d517a035d0abb5cd0919'>European FIRs</a>
